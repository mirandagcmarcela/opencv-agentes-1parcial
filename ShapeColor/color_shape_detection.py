import cv2
import numpy as np
import math


class ColorShapeDetection:

    def __init__(self, image_path1: str, image_path2: str):
        img = cv2.imread(image_path1)
        gray1 = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        canny1 = cv2.Canny(gray1, 10, 150)
        canny1 = cv2.dilate(canny1, None, iterations=1)
        canny1 = cv2.erode(canny1, None, iterations=1)
        imageHSV1 = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
        cnts1, _ = cv2.findContours(canny1, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

        x1, y1, w1, h1 = cv2.boundingRect(cnts1[0])
        imAux1 = np.zeros(img.shape[:2], dtype="uint8")
        imAux1 = cv2.drawContours(imAux1, [cnts1[0]], -1, 255, -1)
        maskHSV = cv2.bitwise_and(imageHSV1, imageHSV1, mask=imAux1)
        name1, area1 = self.shapes_name_and_area_detection(cnts1[0], w1, h1)
        color1 = self.shape_color_detection(maskHSV)
        nameColor1 = name1 + ' - ' + color1 + ' - (' + str(area1) + ')'
        cv2.putText(img, nameColor1, (x1, y1 - 5), 1, 0.5, (0, 255, 0), 1)

        imagen = cv2.imread(image_path2)
        gray = cv2.cvtColor(imagen, cv2.COLOR_BGR2GRAY)
        canny = cv2.Canny(gray, 10, 150)
        canny = cv2.dilate(canny, None, iterations=1)
        canny = cv2.erode(canny, None, iterations=1)
        cnts, _ = cv2.findContours(canny, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        imageHSV = cv2.cvtColor(imagen, cv2.COLOR_BGR2HSV)

        for c in cnts:
            x, y, w, h = cv2.boundingRect(c)
            imAux = np.zeros(imagen.shape[:2], dtype="uint8")
            imAux = cv2.drawContours(imAux, [c], -1, 255, -1)
            maskHSV = cv2.bitwise_and(imageHSV, imageHSV, mask=imAux)
            name, area = self.shapes_name_and_area_detection(c, w, h)
            color = self.shape_color_detection(maskHSV)
            nameColor = name + ' - ' + color + ' - (' + str(area) + ')'+ ' - (Dif: ' + str(float(area/area1)*100) + '%)'
            print(nameColor)
            cv2.putText(imagen, nameColor, (x, y - 5), 1, 0.5, (0, 255, 0), 1)

        cv2.imshow('answer',imagen)
        cv2.imshow('answer1', img)
        cv2.waitKey(0)
        cv2.destroyAllWindows()

    def shape_color_detection(self, imagenHSV):
        rojoBajo1 = np.array([0, 100, 20], np.uint8)
        rojoAlto1 = np.array([10, 255, 255], np.uint8)
        rojoBajo2 = np.array([175, 100, 20], np.uint8)
        rojoAlto2 = np.array([180, 255, 255], np.uint8)

        naranjaBajo = np.array([11, 100, 20], np.uint8)
        naranjaAlto = np.array([19, 255, 255], np.uint8)

        amarilloBajo = np.array([20, 100, 20], np.uint8)
        amarilloAlto = np.array([32, 255, 255], np.uint8)

        verdeBajo = np.array([36, 100, 20], np.uint8)
        verdeAlto = np.array([70, 255, 255], np.uint8)

        violetaBajo = np.array([130, 100, 20], np.uint8)
        violetaAlto = np.array([145, 255, 255], np.uint8)

        rosaBajo = np.array([146, 100, 20], np.uint8)
        rosaAlto = np.array([170, 255, 255], np.uint8)

        maskRojo1 = cv2.inRange(imagenHSV, rojoBajo1, rojoAlto1)
        maskRojo2 = cv2.inRange(imagenHSV, rojoBajo2, rojoAlto2)
        maskRojo = cv2.add(maskRojo1, maskRojo2)

        maskNaranja = cv2.inRange(imagenHSV, naranjaBajo, naranjaAlto)

        maskAmarillo = cv2.inRange(imagenHSV, amarilloBajo, amarilloAlto)

        maskVerde = cv2.inRange(imagenHSV, verdeBajo, verdeAlto)

        maskVioleta = cv2.inRange(imagenHSV, violetaBajo, violetaAlto)

        maskRosa = cv2.inRange(imagenHSV, rosaBajo, rosaAlto)

        cntsRojo = cv2.findContours(maskRojo, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[0]

        cntsNaranja = cv2.findContours(maskNaranja, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[0]

        cntsAmarillo = cv2.findContours(maskAmarillo, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[0]

        cntsVerde = cv2.findContours(maskVerde, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[0]

        cntsVioleta = cv2.findContours(maskVioleta, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[0]

        cntsRosa = cv2.findContours(maskRosa, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[0]

        if len(cntsRojo) > 0:
            color = 'Rojo'

        elif len(cntsNaranja) > 0:
            color = 'Naranja'

        elif len(cntsAmarillo) > 0:
            color = 'Amarillo'

        elif len(cntsVerde) > 0:
            color = 'Verde'

        elif len(cntsVioleta) > 0:
            color = 'Violeta'

        elif len(cntsRosa) > 0:
            color = 'Rosa'

        return color

    def shapes_name_and_area_detection(self, contour, width, height):
        epsilon = 0.01 * cv2.arcLength(contour, True)
        approx = cv2.approxPolyDP(contour, epsilon, True)

        if len(approx) == 3:
            namefig = 'Triangulo'
            area = float((width*height)/2)

        if len(approx) == 4:
            aspect_ratio = float(width) / height
            area = float(width*height)
            if aspect_ratio == 1:
                namefig = 'Cuadrado'
            else:
                namefig = 'Rectangulo'

        if len(approx) == 5:
            area = 0
            namefig = 'Pentagono'

        if len(approx) == 6:
            area = 0
            namefig = 'Hexagono'

        if len(approx) > 10:
            namefig = 'Circulo'
            area = math.pi * width/2

        return namefig, area
