import cv2


class ShapeDetection:

    def __init__(self):
        self.font = cv2.FONT_HERSHEY_COMPLEX

    def polygon_detection(self, path: str):
        img = cv2.imread(path, cv2.IMREAD_GRAYSCALE)
        _, threshold = cv2.threshold(img, 240, 255, cv2.THRESH_BINARY)
        contours, _ = cv2.findContours(threshold, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

        for cnt in contours:
            approx = cv2.approxPolyDP(cnt, 0.01*cv2.arcLength(cnt, True), True)
            cv2.drawContours(img, [approx], 0, (0), 5)
            print(approx.ravel())
            x = approx.ravel()[0]
            y = approx.ravel()[1]

            if len(approx) == 3:
                cv2.putText(img, "Triangulo", (x,y), self.font, 1, (0))

            elif len(approx) == 4:
                x1, y1, w, h = cv2.boundingRect(approx)
                if w == h:
                    cv2.putText(img, "Cuadrado", (x, y), self.font, 1, (0))
                else:
                    cv2.putText(img, "Rectangulo", (x, y), self.font, 1, (0))

            elif len(approx) == 5:
                cv2.putText(img, "Pentagono", (x, y), self.font, 1, (0))

            elif len(approx) == 6:
                cv2.putText(img, "Hexagono", (x, y), self.font, 1, (0))

            elif 6 < len(approx) < 15:
                cv2.putText(img, "Elipse", (x, y), self.font, 1, (0))

            else:
                cv2.putText(img, "Circulo", (x, y), self.font, 1, (0))

        cv2.imshow("shapes_gray", img)
        #cv2.imshow("Threshold", threshold)
        cv2.waitKey(0)
        cv2.destroyAllWindows()
