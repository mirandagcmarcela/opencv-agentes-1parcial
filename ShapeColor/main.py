# from shape_detection import ShapeDetection
from color_shape_detection import ColorShapeDetection


class Main:
    def __init__(self, image_path1: str, image_path2: str = None):
        # ShapeDetection().polygon_detection(image_path1)
        ColorShapeDetection(image_path1, image_path2)

main = Main('redondoNaranja.png', 'figurillas.png')
# main = Main('shapes.png')